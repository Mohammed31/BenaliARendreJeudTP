package com.server.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.client.service.IPostService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.shared.Post;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class PostService extends RemoteServiceServlet implements IPostService {

	public Post getOnePost(int id) {
		Post pos= new Post();
		ObjectMapper mapper = new ObjectMapper();
		
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource webResource = client.resource("https://jsonplaceholder.typicode.com/posts/" + id);
		String postObjetJason = webResource.get(String.class);
		System.out.println(postObjetJason.toString());
		
		try {
			pos = mapper.readValue(postObjetJason, Post.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pos;
	}
	

	@Override
	public List<Post> getAll() {
		ObjectMapper mapper = new ObjectMapper();
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource webResource2 = client.resource("https://jsonplaceholder.typicode.com/posts");
		String listOfPostJson = webResource2.get(String.class);
		List<Post> list = new ArrayList<Post>();
		
		try {
			list = mapper.readValue(listOfPostJson, new TypeReference<List<Post>>() {
			});
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
		
		
	}

	
	
	
	
}
