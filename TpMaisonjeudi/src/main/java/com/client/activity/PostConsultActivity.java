package com.client.activity;

import com.client.ClientFactory;
import com.client.place.PostConsultPlace;
import com.client.service.IPostService;
import com.client.service.IPostServiceAsync;
import com.client.view.IpostConsultView;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.shared.Post;

public class PostConsultActivity extends AbstractActivity {

	private ClientFactory clientFactory;
	private IpostConsultView postConsultView;
	private int idPost;
	private static final IPostServiceAsync POST_SERVICE = GWT.create(IPostService.class);

	public PostConsultActivity(PostConsultPlace place, ClientFactory clientFactory) {

		this.clientFactory = clientFactory;
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {

		postConsultView = clientFactory.getConsultView();
		postConsultView.reset();
		panel.setWidget(postConsultView.asWidget());
		POST_SERVICE.getOnePost(idPost, new AsyncCallback<Post>() {
			
			@Override
			public void onSuccess(Post result) {
				postConsultView.display(result);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				GWT.log("pas Bon !!!");
				
			}
		});
		
	}

}
