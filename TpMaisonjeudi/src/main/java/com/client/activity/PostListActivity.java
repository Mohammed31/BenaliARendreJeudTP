package com.client.activity;

import java.util.List;

import com.client.ClientFactory;
import com.client.place.PostConsultPlace;
import com.client.place.PostsListPlace;
import com.client.service.IPostService;
import com.client.service.IPostServiceAsync;
import com.client.view.IpostListView;
import com.client.view.IpostListView.Presenter;
import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.shared.Post;

public class PostListActivity extends AbstractActivity implements Presenter {

	private ClientFactory clientFactory;
	private IpostListView postsListView;
	private static final IPostServiceAsync USER_SERVICE = GWT.create(IPostService.class);

	public PostListActivity(PostsListPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {

		postsListView = clientFactory.getListView();
		postsListView.setPresenter(this);
		postsListView.reset();
		panel.setWidget(postsListView.asWidget());
		findAll();

	}

	@Override
	public void display(int idPost) {
		PostConsultPlace postConsultPlace = new PostConsultPlace();
		postConsultPlace.setIdPost(idPost);
		clientFactory.getPlaceController().goTo(postConsultPlace);
	}

	void findAll() {
		USER_SERVICE.getAll(new AsyncCallback<List<Post>>() {

			@Override
			public void onSuccess(List<Post> result) {
				postsListView.displayPosts(result);

			}

			@Override
			public void onFailure(Throwable caught) {
				
				Window.alert("KO");
				
			}
		});

	}

}
