package com.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.shared.Post;

public interface IPostServiceAsync {
	
	void getAll(AsyncCallback<List<Post>> callback);
	
	void getOnePost(int idPost,AsyncCallback<Post> callback);
	

}
