package com.client.service;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.shared.Post;


@RemoteServiceRelativePath("posts")
public interface IPostService extends RemoteService {
	
	public Post getOnePost(int idpost);
	public List<Post> getAll();

}
