package com.client;

import java.util.List;

import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.Jumbotron;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;
import org.gwtbootstrap3.client.ui.html.Div;

import com.client.mvp.AppActivityMapper;
import com.client.mvp.AppHistoryMapper;
import com.client.place.PostsListPlace;
import com.client.service.IPostService;
import com.client.service.IPostServiceAsync;
import com.gargoylesoftware.htmlunit.javascript.host.Window;
import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.server.service.PostService;
import com.shared.Post;

public class TpMaisonjeudi implements EntryPoint {

	private static ClientFactory clientFactory;
	private ContentPanel dynamicPanel = new ContentPanel();
	private Div staticContentPanel;

	public static ClientFactory getClientFactory() {
		if (clientFactory == null)
			clientFactory = GWT.create(ClientFactory.class);
		return clientFactory;
	}

	@Override
	public void onModuleLoad() {

		clientFactory = getClientFactory();
		initGui();
		PlaceController placeController = getClientFactory().getPlaceController();

		// lancement du activityManager pour le widget principale avec notre
		// //activityMapper
		ActivityMapper activityMapper = new AppActivityMapper(getClientFactory());
		ActivityManager activityManager = new ActivityManager(activityMapper, getClientFactory().getEventBus());
		activityManager.setDisplay(dynamicPanel);

		AppHistoryMapper historyMapper = GWT.create(AppHistoryMapper.class);
		//GWT.log("Test1");
		PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
		//GWT.log("Test2");
		historyHandler.register(placeController, getClientFactory().getEventBus(), new PostsListPlace());
		//GWT.log("Test3");
		historyHandler.handleCurrentHistory();
		
		
//		 IPostServiceAsync service = GWT.create(IPostService.class);
//		 service.getAll(new AsyncCallback<List<Post>>() {
//		
//		 @Override
//		 public void onSuccess(List<Post> result) {
//		 consolelog(result.get(0).toString());
//		 }
	
//		 @Override
//		 public void onFailure(Throwable caught) {
//		 consolelog("NOPEooooooooooooooooooo");
//		 }
//		 });
	}

//	 native void consolelog(String message)/*-{
//	 console.log(message)
//	 }-*/;

	private void initGui() {
		staticContentPanel = new Div();

		Jumbotron jumbotron = new Jumbotron();
		Container container = new Container();
		Heading heading = new Heading(HeadingSize.H1);
		heading.setText("la liste des postes qui ma fatiguer");
		container.add(heading);
		jumbotron.add(container);
		staticContentPanel.add(jumbotron);
		staticContentPanel.add(dynamicPanel);

		RootPanel.get().add(staticContentPanel);
	}

	private class ContentPanel extends VerticalPanel implements AcceptsOneWidget {

		public ContentPanel() {
			super();
			setWidth("100%");
		}

	
		
		@Override
		public void setWidget(IsWidget w) {
			if (w != null) {
				clear();
				add(w.asWidget());
			}
		}
	}
}
