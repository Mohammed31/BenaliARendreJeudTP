package com.client;

import com.client.view.IpostConsultView;
import com.client.view.IpostListView;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;

public interface ClientFactory {
	
	EventBus getEventBus();
	IpostConsultView getConsultView();
	IpostListView getListView();
	PlaceController getPlaceController();

}
