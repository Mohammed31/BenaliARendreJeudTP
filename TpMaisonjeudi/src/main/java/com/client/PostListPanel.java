package com.client;

import java.util.List;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.constants.ButtonSize;
import org.gwtbootstrap3.client.ui.constants.ButtonType;

import com.client.place.PostConsultPlace;
import com.client.view.DetailPresenter;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.shared.Post;

public class PostListPanel extends VerticalPanel {

	private DetailPresenter presenter;

	public PostListPanel() {
		super();

		setWidth("100%");
	}

	public void reset() {
		clear();
	}

	public void setPosts(List<Post> posts) {

		reset();
		int i = 0;
		Grid grid = new Grid(posts.size(), 4);
		grid.setWidth("100%");

		for (Post p : posts) {
			grid.setWidget(i, 0, new Label(p.getTitle()));
			grid.setWidget(i, 1, new Label(Integer.toString(p.getUserid())));
			grid.setWidget(i, 2, new Label(p.getBody()));

			Button detailButton = new Button("Detail");
			detailButton.setType(ButtonType.PRIMARY);
			detailButton.setSize(ButtonSize.SMALL);
			final int idPost = p.getId();
			detailButton.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					PostConsultPlace postConsultPlace = new PostConsultPlace();
					postConsultPlace.setIdPost(idPost);
					TpMaisonjeudi.getClientFactory().getPlaceController().goTo(postConsultPlace);
					}
			});

			grid.setWidget(i, 2, detailButton);
			i++;
			

		}
		add(grid);
	
		
		
	}

	
	
	
	public void setPresenter(DetailPresenter presenter) {
		this.presenter = presenter;
	}
	
	

}
