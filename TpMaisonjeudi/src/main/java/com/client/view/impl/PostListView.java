package com.client.view.impl;

import java.util.List;

import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;

import com.client.PostListPanel;
import com.client.view.IpostListView;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.shared.Post;

public class PostListView extends VerticalPanel implements IpostListView {

	private Presenter presenter;
	private PostListPanel postListPanel;

	public PostListView() {
		super();

		setWidth("100%");
		getElement().getStyle().setMargin(5, Unit.PX);
		getElement().getStyle().setBorderColor("red");
		getElement().getStyle().setBorderWidth(4, Unit.PCT);
		getElement().getStyle().setBorderStyle(BorderStyle.DOTTED);

		Heading title = new Heading(HeadingSize.H2);
		title.setText("liste postes ");
		add(title);

		postListPanel = new PostListPanel();
		add(postListPanel);
		
		
	}

	@Override
	public void reset() {
		postListPanel.reset();
	}

	@Override
	public void displayPosts(List<Post> posts) {
		postListPanel.reset();
		postListPanel.setPosts(posts);
	}

	@Override
	public void setPresenter(Presenter presenter) {

		this.presenter = presenter;
		postListPanel.setPresenter(presenter);

	}

}
