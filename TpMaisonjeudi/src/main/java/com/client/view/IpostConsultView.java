package com.client.view;

import com.google.gwt.user.client.ui.IsWidget;
import com.shared.Post;

public interface IpostConsultView extends IsWidget {

	void reset();

	void display(Post post);

	

}
