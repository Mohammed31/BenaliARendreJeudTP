package com.client.view;

import java.util.List;

import com.google.gwt.user.client.ui.IsWidget;
import com.shared.Post;


public interface IpostListView extends IsWidget {
	
	void reset();
	void displayPosts(List<Post> posts);
	void setPresenter(Presenter presenter);
	public interface Presenter extends DetailPresenter {
			
	}

}
