package com.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class PostsListPlace extends Place {
	
	public static class Tokenizer implements PlaceTokenizer<PostsListPlace> {

		@Override
		public PostsListPlace getPlace(String token) {
			
			return new PostsListPlace();
		}

		@Override
		public String getToken(PostsListPlace place) {
			return "";
		}
		
	}

}
