package com.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class PostConsultPlace extends Place {

	private int idPost;

	public int getIdPost() {
		return idPost;
	}

	public void setIdPost(int idPost) {
		this.idPost = idPost;
	}

	public static class Tokenizer implements PlaceTokenizer<PostConsultPlace> {

		@Override
		public PostConsultPlace getPlace(String token) {
			PostConsultPlace postConsultPlace = new PostConsultPlace();
			postConsultPlace.setIdPost(Integer.parseInt(token));
			return postConsultPlace;
		}

		@Override
		public String getToken(PostConsultPlace place) {
			return String.valueOf(place.getIdPost());
		}

	}

}
