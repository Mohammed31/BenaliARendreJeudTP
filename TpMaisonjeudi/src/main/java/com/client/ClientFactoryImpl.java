package com.client;

import com.client.view.IpostConsultView;
import com.client.view.IpostListView;
import com.client.view.impl.PostConsultView;
import com.client.view.impl.PostListView;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;

public class ClientFactoryImpl implements ClientFactory {

	private static final EventBus EVENT_BUS = new SimpleEventBus();
	private static final PlaceController PLACE_CONTROLLER = new PlaceController(EVENT_BUS);
	
	
	@Override
	public EventBus getEventBus() {
		
		return EVENT_BUS;
	}

	@Override
	public IpostConsultView getConsultView() {
		
		return new PostConsultView();
	}

	@Override
	public IpostListView getListView() {
		
		return  new PostListView();
	}

	@Override
	public PlaceController getPlaceController() {
		
		return PLACE_CONTROLLER;
	}

}
