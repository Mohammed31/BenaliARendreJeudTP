package com.client.mvp;

import com.client.ClientFactory;
import com.client.activity.PostConsultActivity;
import com.client.activity.PostListActivity;
import com.client.place.PostConsultPlace;
import com.client.place.PostsListPlace;
import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

public class AppActivityMapper implements ActivityMapper {

	private ClientFactory clientFactory;

	public AppActivityMapper(ClientFactory clientFactory) {
		super();
		this.clientFactory = clientFactory;
	}

	@Override
	public Activity getActivity(Place place) {
		if (place instanceof PostConsultPlace)
			return new PostConsultActivity((PostConsultPlace) place, clientFactory);
		else
			return new PostListActivity((PostsListPlace) place, clientFactory);
	}

}
