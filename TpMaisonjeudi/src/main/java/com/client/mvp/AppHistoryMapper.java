package com.client.mvp;


import com.client.place.PostConsultPlace;
import com.client.place.PostsListPlace;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

@WithTokenizers({PostConsultPlace.Tokenizer.class,PostsListPlace.Tokenizer.class})
public interface AppHistoryMapper extends PlaceHistoryMapper{

}
